var classlab3__task__user_1_1task__user =
[
    [ "__init__", "classlab3__task__user_1_1task__user.html#aafbb79a7246e55ecfcef6e761e729e7d", null ],
    [ "intro", "classlab3__task__user_1_1task__user.html#ae9fbb805e584a10b1ae928c406bca90b", null ],
    [ "transition_to", "classlab3__task__user_1_1task__user.html#a8c610f4e3cdcc1f37b428cba01e2884d", null ],
    [ "diff_array", "classlab3__task__user_1_1task__user.html#ab6e78513fa31e08730f2c99338f9ab0b", null ],
    [ "duty", "classlab3__task__user_1_1task__user.html#ab5de0d2fde1ff79071da7d9698e1809d", null ],
    [ "encoder1_share", "classlab3__task__user_1_1task__user.html#a87d1b00ed5c4451c5696f8a8065d00f6", null ],
    [ "encoder2_share", "classlab3__task__user_1_1task__user.html#a33a745b617b71d7017eebf8e18408d00", null ],
    [ "fault_share", "classlab3__task__user_1_1task__user.html#adc323931c34430b3d464245f727a766b", null ],
    [ "motor1_share", "classlab3__task__user_1_1task__user.html#ace5517586b2327923e0c26f09d462c91", null ],
    [ "motor2_share", "classlab3__task__user_1_1task__user.html#a50e4da42f530d3c09d743435e612c100", null ],
    [ "next_time", "classlab3__task__user_1_1task__user.html#a8c17c66d337b86c45dd956bd57e24de0", null ],
    [ "period", "classlab3__task__user_1_1task__user.html#aa460e8fd5c4fa883b3c34525649a127a", null ],
    [ "pos_array", "classlab3__task__user_1_1task__user.html#a2415c3a631384fe32433377f25f62aba", null ],
    [ "print_count", "classlab3__task__user_1_1task__user.html#adecec6a06e97f661d02216d820d4f080", null ],
    [ "runs", "classlab3__task__user_1_1task__user.html#a6fd6910478ed542b9f70c8d6a312e598", null ],
    [ "ser", "classlab3__task__user_1_1task__user.html#a44a80b90047f5012f1b3802b52606fa8", null ],
    [ "speed_array", "classlab3__task__user_1_1task__user.html#a4c05747f2de4331bc461a3735791642b", null ],
    [ "state", "classlab3__task__user_1_1task__user.html#a561fe54ae9a63c50c2b28a0beaf6fb37", null ],
    [ "time_array", "classlab3__task__user_1_1task__user.html#a3b339dec6316ef0fa5fdcc45a3d6ad3b", null ],
    [ "zflag1", "classlab3__task__user_1_1task__user.html#a91477b50c0f17f33345de31b839ac60f", null ],
    [ "zflag2", "classlab3__task__user_1_1task__user.html#ae944566f792d8e47097b86e74f6af733", null ]
];